# Clap

Flat file sound library, sorting and writing [ID3](https://en.wikipedia.org/wiki/ID3).

# What it does

- No database, all data are stored in the sound file
- Tags that can be filtered
- Live Search
- Sort by duration
- Live write ID3 tags on mp3 including comments and titles, more to come

# Good to know

- Needs php 7+
- Make sure `sounds/` is writable and contains sound files
- Use the app at your own risk, it may contain bugs and erase your existing ID3 data
- The app will use the ID3 `comment` field to store keywods
- Both OGG and MP3 ID3 data can be written and read. If you chose to work with OGG, `vorbis-tools` is needed on the system `apt-get install -y vorbis-tools`.

![clap screenshot](clap.png)

# Licence

[AGPL](https://www.gnu.org/licenses/agpl-3.0.en.html)
