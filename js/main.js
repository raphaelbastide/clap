let d = document
let b = d.body
let tagList = []
var options = {
  valueNames: [
    'filename',
    { name: 'comment', attr: 'data-comment' },
    { name: 'title', attr: 'value' },
    { name: 'artist', attr: 'value' },
    { name: 'album', attr: 'value' },
    { name: 'year', attr: 'value' },
    'playtime' ],
    page: 1000,
  };
let hackerList = new List('hacker-list', options);

// create taglist
update()

// Saving data
let inputs = d.querySelectorAll('li input, li textarea')
for (var i = 0; i < inputs.length; i++) {
  saveComment(inputs[i]) // save all to store tags on load
  inputs[i].addEventListener('focusout', (event) => {
    let that = event.target
    // save comment
    if (that.name == 'comment')
      saveComment(that)
    let form = that.parentNode
    form.classList.add('saved')
    setTimeout(function () {
      form.classList.remove('saved')
    }, 200);
    return submitForm(form)
  });
}

function saveComment(textarea){
  let comment = textarea.value
  textarea.setAttribute('data-comment', comment)
  update()
}

function update(){
  // empty taglist
  tagList = []
  // empty datalist options
  d.querySelector('#datalist-taglist').innerHTML = ""
  let textareas = d.querySelectorAll('.comment')
  for (var i = 0; i < textareas.length; i++) {
    let comment = textareas[i].value
    let tags = findHashtags(comment)
    for (var j = 0; j < tags.length; j++) {
      tagList.push(tags[j])
    }
    tagList = removeDuplicate(tagList)
  }
  d.querySelector('.taglist').innerHTML = ""
  tagList.forEach(tagHash => {
    // fill taglist
    tagEl = d.createElement("span")
    tagEl.innerText = tagHash
    let tag = tagHash.replace("#","")
    d.querySelector('.taglist').insertAdjacentElement("beforeend", tagEl)
    console.log(tag);
    tagEl.onclick = function(){
      d.querySelector('.search').value = tag
      hackerList.search(tag)
    }    
    // fill datalist taglist with <option>
    let option = d.createElement('option')
    option.setAttribute('value', tag)
    d.querySelector('#datalist-taglist').appendChild(option)
  });

  // update js list
  hackerList.refresh()
}

// Player
let playBtns = d.querySelectorAll('.play-button')
for (var i = 0; i < playBtns.length; i++) {
  button = playBtns[i]
  let playerCol = button.parentNode.parentNode.querySelector('.col.audio')
  let player = button.parentNode.parentNode.querySelector('audio')
  player.onended = function(){
    relativeBtn = this.parentNode.querySelector('.play-button')
    relativeBtn.classList.remove('playing')
    relativeBtn.innerText = "▶"
  }
  button.onclick = function(){
    if (!this.classList.contains('playing')) {
      playerCol.style.display = "block"
      player.play()
      this.classList.add('playing')
      this.innerText = "▌▌"
    }else {
      player.pause()
      player.currentTime = 0;
      this.classList.remove('playing')
      this.innerText = "▶"
    }
  }
}

// Toggle taglist
let tagListBtn = d.querySelector('.taglist-button')
tagListBtn.onclick = function(){
  if (b.classList.contains('show-taglist')) {
    b.classList.remove('show-taglist')
  }else {
    b.classList.add('show-taglist')
  }
}

// Form
function submitForm(oFormElement){
  var xhr = new XMLHttpRequest();
  xhr.onload = function(){
    console.log(xhr.responseText);
  }
  xhr.open (oFormElement.method, oFormElement.action, true);
  xhr.send (new FormData (oFormElement));
  return false;
}

// Utils
function findHashtags(searchText) {
  var regexp = /\B\#\w\w+\b/g
  result = searchText.match(regexp);
  if (result) {
    return result;
  } else {
    return false;
  }
}
function removeDuplicate(a) {
  return a.sort().filter(function(item, pos, ary) {
    return !pos || item != ary[pos - 1];
  });
}
