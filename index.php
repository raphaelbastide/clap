<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Clap</title>
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="shortcut icon" href="img/favicon.png"/>
    <meta name="description" content="A sound library" />
    <meta name="viewport" content="width=device-width">
    <meta property="og:image" content="" />
 </head>
  <body>
    <div id="hacker-list">
      <header class="main-header">
        <h1 class="col">clap</h1>
        <input class="search col" list="datalist-taglist" placeholder="search"/>
        <datalist id="datalist-taglist">
        </datalist>
        <button class="taglist-button col">tags</button>
        <a class="col about" href="https://gitlab.com/raphaelbastide/clap">about</a>
      </header>
      <div class="taglist"></div>
      <header class="table-header">
        <span class="sort col" data-sort="filename">filename</span>
        <span class="sort col" data-sort="title">title</span>
        <span class="sort col" data-sort="artist">artist</span>
        <!-- <span class="sort col" data-sort="genre">genre</span> -->
        <span class="sort col small-col" data-sort="year">year</span>
        <span class="sort col" data-sort="album">album</span>
        <span class="col">comment</span>
        <span class="sort col" data-sort="playtime">duration</span>
        <span class="col small-col">play</span>
      </header>
      <ul class="list">
        <?php
        require_once('functions.php');
        require_once('lib/getid3/getid3.php');
        $directory = "sounds";
        
        // change the format here, either "ogg" or "mp3"
        $format = "mp3";

        if ($format == "mp3") {
          $files = rglob($directory."/{*.mp3}", GLOB_BRACE);
        }elseif ($format == "ogg") {
          $files = rglob($directory."/{*.ogg}", GLOB_BRACE);
        }
        foreach ($files as $file) {
          $getID3 = new getID3;
          $file_dir = dirname($file);
          $file_name = basename($file);
          $file_slug = basename($file_dir);
          $ID3data = $getID3->analyze($file);
          // MP3
          if ($format == "mp3") {
            if (isset($ID3data['tags']['id3v2']['title'][0])) {
              $title = $ID3data['tags']['id3v2']['title'][0];
            }else {$title = "";}
            if (isset($ID3data['tags']['id3v2']['artist'][0])) {
              $artist = $ID3data['tags']['id3v2']['artist'][0];
            }else {$artist = "";}
            if (isset($ID3data['tags']['id3v2']['album'][0])) {
              $album = $ID3data['tags']['id3v2']['album'][0];
            }else {$album = "";}
            if (isset($ID3data['tags']['id3v2']['genre'][0])) {
              $genre = $ID3data['tags']['id3v2']['genre'][0];
            }else {$genre = "";}
            if (isset($ID3data['tags']['id3v2']['year'][0])) {
              $year = $ID3data['tags']['id3v2']['year'][0];
            }else {$year = "";}
            if (isset($ID3data['tags']['id3v2']['comment'][0])) {
              $comment = $ID3data['tags']['id3v2']['comment'][0];
            }else {$comment = "";}
          }else if($format == "ogg"){
            // OGG
            if (isset($ID3data['tags']['vorbiscomment']['title'][0])) {
              $title = $ID3data['tags']['vorbiscomment']['title'][0];
            }else {$title = "";}
            if (isset($ID3data['tags']['vorbiscomment']['artist'][0])) {
              $artist = $ID3data['tags']['vorbiscomment']['artist'][0];
            }else {$artist = "";}
            if (isset($ID3data['tags']['vorbiscomment']['album'][0])) {
              $album = $ID3data['tags']['vorbiscomment']['album'][0];
            }else {$album = "";}
            // if (isset($ID3data['tags']['vorbiscomment']['genre'][0])) {
            //   $genre = $ID3data['tags']['vorbiscomment']['genre'][0];
            // }else {$genre = "";}
            if (isset($ID3data['tags']['vorbiscomment']['year'][0])) {
              $year = $ID3data['tags']['vorbiscomment']['year'][0];
            }else {$year = "";}
            if (isset($ID3data['tags']['vorbiscomment']['description'][0])) {
              $comment = $ID3data['tags']['vorbiscomment']['description'][0];
            }else {$comment = "";}
          }

          // echo '<pre>';
          // echo print_r($ID3data);
          // echo '</pre>';
          $playtime = round($ID3data['playtime_seconds'], 2);
          echo "<li>";
          echo "<form autocomplete='off' id='form-$file_slug' method='POST' action='write.php' onsubmit='return submitForm(this);' >";
          echo "<input hidden type='text' name='file' value='$file'>";
          echo "<input hidden type='text' name='format' value='$format'>";
          echo "<p class='col filename'>$file_name</p>";
          echo "<input class='col title' type='text' name='title' value='$title'>";
          echo "<input class='col artist' type='text' name='artist' value='$artist'>";
          echo "<input class='col year small-col' type='text' name='year' value='$year'>";
          echo "<input class='col album' type='text' name='album' value='$album'>";
          // echo "<input class='col genre' type='text' name='genre' value='$genre'>";
          echo "<textarea class='col comment' name='comment'>$comment</textarea>";
          echo "<p class='col playtime' title='".$playtime."s'><meter value='$playtime' min='0' max='5'></meter></p>";
          echo "<p class='col play-button button small-col' title='play'>▶</p>";
          echo "<audio controls class='col audio'><source src='$file' type='audio/mpeg'></audio>";
          echo "</form>";
          echo "</li>";
        }
        ?>
      </ul> <!-- end of .list -->
    </div> <!-- end of #hacker-list -->
    <script src="js/list.js" charset="utf-8"></script>
    <script src="js/main.js" charset="utf-8"></script>
  </body>
</html>
