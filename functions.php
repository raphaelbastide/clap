<?php 
// Recursive Glob function ; Does not support flag GLOB_BRACE
function rglob($pattern, $flags = 0) {
  $files = glob($pattern, $flags);
  foreach (glob(dirname($pattern).'/*', GLOB_ONLYDIR|GLOB_NOSORT) as $dir) {
    $files = array_merge($files, rglob($dir.'/'.basename($pattern), $flags));
  }
  return $files;
}

// List Classes
function listClass($classArray){
  $classes = "";
  if (isset($classArray) && $classArray != ""){
    for ($i=0; $i < count($classArray) ; $i++) {
      $classes .= $classArray[$i]." ";
    }
    return $classes;
  }
}

// Extract body
function getBodyContent($page){
  $file = file_get_contents($page);
  $body = preg_replace("/.*<main[^>]*>|<\/main>.*/si", "", $file);
  return $body;
}

// Writing
function writxt($destination, $content){
  file_put_contents($destination, $content);
  echo $destination;
  fclose($destination);
  header("Location: ../");
  die();
};
