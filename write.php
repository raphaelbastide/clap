<?php

require_once('functions.php');
require_once('lib/getid3/getid3.php');
require_once('lib/getid3/write.php');

$file = $_POST["file"];
$format = $_POST["format"];
$title = $_POST["title"];
$artist = $_POST["artist"];
$album = $_POST["album"];
// $genre = $_POST["genre"];
$year = $_POST["year"];
$comment = $_POST["comment"];

// Initialize getID3 tag-writing module
$tagwriter = new getid3_writetags;
$tagwriter->filename = $file;
// MP3
if ($format == "ogg") {
  // OGG (requires vorbis-tools installed on the system)
  $tagwriter->tagformats = array('vorbiscomment');
}elseif ($format == "mp3") {
  $tagwriter->tagformats = array('id3v1', 'id3v2.3');
}

// set various options (optional)
$tagwriter->overwrite_tags = true;
$tagwriter->tag_encoding = 'UTF-8';
$tagwriter->remove_other_tags = true;

// populate data array
if ($format == "ogg") {
  $TagData['description'][] = $comment;
}elseif ($format == "mp3") {
  $TagData['comment'][] = $comment;
}
$TagData['title'][] = $title;
$TagData['artist'][] = $artist;
$TagData['album'][] = $album;
// $TagData['genre'][] = $genre;
$TagData['year'][] = $year;
$tagwriter->tag_data = $TagData;

// write tags
if ($tagwriter->WriteTags()) {
  echo 'Successfully wrote tags';
  if (!empty($tagwriter->warnings)) {
    echo 'There were some warnings:<br>'.implode('<br><br>', $tagwriter->warnings);
  }
} else {
  echo 'Failed to write tags!<br>'.implode('<br><br>', $tagwriter->errors);
}

?>
